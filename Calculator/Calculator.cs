﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class Calculator
    {
        public Calculator()
        {

        }

        public int Compute(string number_string)
        {
            if (String.IsNullOrEmpty(number_string))
                return 0;
            if (number_string.Length == 1)
                return Int32.Parse(number_string);
            else
                return ProcessString(number_string);
        }

        public int ProcessString(string number_string)
        {
            string[] separators;
            if(HasCustomSeparators(number_string))
            {
                var splitted_string = number_string.Split(new string[] { "\n" }, StringSplitOptions.None);
                var separators_string = splitted_string[0];
                var raw_string = splitted_string[1];
                separators = FindSeparators(number_string);
                return CalculateValue(raw_string, separators);
            }
            else
            {
                separators = new string[] { "\n", "," };
                return CalculateValue(number_string, separators);
            }
        }

        public string[] FindSeparators(string number_string)
        {
            string separators_string = number_string.Remove(0, 2);
            List<string> separators = new List<string>();

            if (separators_string.Contains("["))
            {
                while (separators_string.StartsWith("["))
                {
                    int end_index = separators_string.IndexOf(']');
                    separators.Add(separators_string.Substring(1, end_index - 1));
                    separators_string = separators_string.Remove(0, end_index + 1);
                }
            }
            else
                separators.Add(separators_string[0].ToString());
            return separators.ToArray();
        }

        public int CalculateValue(string raw_string, string[] separators)
        {
            var arr = raw_string.Split(separators, StringSplitOptions.None);
            var sum = 0;
            foreach (string s in arr)
            {
                var n = Int32.Parse(s);
                if (n < 0)
                    throw new ArgumentException();
                if (n > 1000)
                    continue;
                sum += n;
            }
            return sum;
        }

        public bool HasCustomSeparators(string s)
        {
            if (s.StartsWith("//"))
                return true;
            return false;
        }
    }
}
