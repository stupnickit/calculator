using NUnit.Framework;
using Calculator;
using System;

namespace Tests
{
    public class CalculatorTests
    {
        Calculator.Calculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new Calculator.Calculator();
        }

        [Test]
        public void Compute_EmptyString_Return0()
        {
            //given
            var empty_string = "";
            var expected_result = 0;

            //when
            var result = calculator.Compute(empty_string);

            //then
            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_OneNumber_NumberValue()
        {
            var one_number = "4";
            var expected_result = 4;

            var result = calculator.Compute(one_number);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_TwoNumbersCommaSeparated_Sum()
        {
            var sum_of_two = "4,2";
            var expected_result = 6;

            var result = calculator.Compute(sum_of_two);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_TwoNumbersNewLineSeparated_Sum()
        {
            var sum_of_two = "4\n2";
            var expected_result = 6;

            var result = calculator.Compute(sum_of_two);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_ThreeNumbersSeparated_Sum()
        {
            var sum_of_three = "14\n4,3";
            var expected_result = 21;

            var result = calculator.Compute(sum_of_three);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_NegativeNumber_ThrowExecption()
        {
            var exception_sum = "-2,4,6";

            Assert.That(() => calculator.Compute(exception_sum),
                Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void Compute_NumbersGreaterThan1000_Ignore()
        {
            var ignore_one = "1433,44,3000";
            var expected_result_one = 44;

            var ignore_two = "1111,4400,9999";
            var expected_result_two = 0;


            var result_one = calculator.Compute(ignore_one);
            var result_two = calculator.Compute(ignore_two);

            Assert.AreEqual(result_one, expected_result_one);
            Assert.AreEqual(result_two, expected_result_two);
        }

        [Test]
        public void Compute_ThreeNumbersHashSeparated_ReturnSum()
        {
            var new_separator_string = "//#\n4#5#6";
            var expected_result = 15;

            var result = calculator.Compute(new_separator_string);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_ThreeNumbersDoubleHashSeparated_ReturnSum()
        {
            var new_separator_string = "//[##]\n5##2##15";
            var expected_result = 22;

            var result = calculator.Compute(new_separator_string);

            Assert.AreEqual(result, expected_result);
        }

        [Test]
        public void Compute_ThreeNumbersCustomSeparated_ReturnSum()
        {
            var new_separator_string = "//[##][,][???]\n14##3???7";
            var expected_result = 24;

            var result = calculator.Compute(new_separator_string);

            Assert.AreEqual(result, expected_result);
        }
    }
}